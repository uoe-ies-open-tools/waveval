import sys
import os

sys.path.insert(0, os.path.abspath('../waveval'))
sys.path.insert(0, os.path.abspath('../'))

project = 'WaveVal'
copyright = '2022'
author = 'Chris Old'

extensions = ['sphinx.ext.duration', 
              'sphinx.ext.autodoc', 
              'sphinx.ext.autosummary', 
              'sphinx.ext.coverage', 
              'sphinx.ext.napoleon', 
              'sphinx.ext.autosectionlabel', 
              'sphinx.ext.mathjax', 
              'sphinx.ext.viewcode', 
              'sphinx.ext.intersphinx', 
              'sphinx.ext.todo', ]

templates_path = ['_templates']
language = 'en'

source_suffix = '.rst'
master_doc = 'index'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'
html_theme = 'bizstyle'
autoclass_content = 'both'
intersphinx_mapping = {'python': ('https://docs.python.org/3/', None),
                       'sphinx': ('https://www.sphinx-doc.org/en/master/', None),}

todo_include_todos = True

# Options for LaTeX output
# ------------------------

# The paper size ('letter' or 'a4').
latex_paper_size = 'a4'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, document class [howto/manual]).
latex_documents = [
  ('index', 'waveval.tex', 'WaveVal Documentation',
   'Dr. Chris Old', 'manual', False),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = './_static/banner-large.png'

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

latex_elements = {
# The paper size ('letter' or 'a4').
'papersize': 'a4',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# necessary for unicode charactacters in pdf output
'inputenc': '',
'utf8extra': '',

# remove blank pages (between the title page and the TOC, etc.)
'classoptions': ',openany,oneside',
'babel' : '\\usepackage[shorthands=off]{babel}',

# Additional stuff for the LaTeX preamble.
'preamble': r'''
  \usepackage{hyperref}
  \setcounter{tocdepth}{3}
'''
}

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
latex_use_index = True
latex_use_modindex = True
#

# -- Options for PDF output ---------------------------------------

    # Grouping the document tree into PDF files. List of tuples
    # (source start file, target name, title, author).
pdf_documents = [
    ('index', 'WaveVal', 'WaveVal Documentation', 'Dr. Chris Old'),
]

# A comma-separated list of custom stylesheets. Example:
pdf_stylesheets = ['sphinx','kerning','a4']

# Create a compressed PDF
# Use True/False or 1/0
# Example: compressed=True
#pdf_compressed=False

# A colon-separated list of folders to search for fonts. Example:
# pdf_font_path=['/usr/share/fonts', '/usr/share/texmf-dist/fonts/']

# Language to be used for hyphenation support
pdf_language="en_EN"

# If false, no index is generated.
pdf_use_index = True

# If false, no modindex is generated.
pdf_use_modindex = True

# If false, no coverpage is generated.
#pdf_use_coverpage = True

