Examples
========

Current example code is located on GitLab: `WaveVal examples`_.

.. _WaveVal examples: https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/WaveVal/tree/main/examples 

The example scripts demonstrate how the `WaveVal`_ tools can be used to process a set of `ResourceCode`_ hindcast model 
data extracts against `CMEMS InSituTAC`_ wavebuoy data. The data for these example cases are included in the ``./examples/data`` subdirectory. The model extract data are in ``./data/RSCD_HINDCAST/2017`` and the corresponding wavebuoy 
data are in the ``./data/INSITU_GLO_WAVE/MO`` subdirectory. For ``Example 1`` and ``Example 2`` the 1-D frequency spectra 
data store in the ``/FREQ_NC`` subdirectory within each month directories (*e.g.* ``./data/RSCD_HINDCAST/2017/01/FREQ_NC`` 
contains the 1-D spectral data for January 2017).

``Example 1`` shows how to create a matchup database for a set of model and wavebuoy records and how to convert the 
database records to a CSV file that can be used by the :ref:`Validation` module.

``Example 2`` shows how to generate validation statistics for the records in a CSV input file. This example uses the output 
from Example 1, but the corresponding file is included in the ``./data/MATCHUPDB`` subdirectory.

.. note::

  The example scripts use absolute paths, so the examples must be run from within the ``./examples`` subdirectory.

.. _WaveVal: https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/rcode_tools
.. _ResourceCode: https://resourcecode.ifremer.fr/
.. _CMEMS InSituTAC: http://www.marineinsitu.eu/dashboard/

Example 1: Create a matchup database
####################################

.. literalinclude:: ../examples/rscd_tools_example_01_matchupdb.py
  :linenos:
  :caption: rscd_tools_example_01_matchupdb.py

Example 2: Calculate validation parameters
##########################################

.. literalinclude:: ../examples/rscd_tools_example_02_validate.py
  :linenos:
  :caption: rscd_tools_example_02_validate.py





