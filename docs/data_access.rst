External data archives
======================

`ResourceCode`_  - ResourceCode Hindcast Data Archive
#####################################################
The ResourceCode hindcast data are served from the sextant data service operated by 
*Laboratoire d'Oceanographie Physique et Spatiale* at IFREMER. 

The ResourceCode hindcase database can be accessed using the following link: `HINDCAST`_.

On the righthand side of this page there are options to access the data in the box 
*Acces aux donnees*. The button with the download symbol will take you to a registration page, 
once registered a set of links are provided. Copy the ftp link to get the information required 
to access the ftp server.

The ftp link provided has the form:

        ftp://username:password@server

The three parts (`username`, `password`, `server`) can be used to create a link to the archive 
through a suitable software tool (e.g. `PuTTY`_, `WinSCP`_, `FileZilla`_, `MobaXTerm`_, etc.).

The archive is very large, so do not attempt to download the full archive. Use an ftp tool to 
select the required files. 

The data are organized by year, then month, then data type. The main hindcast data types are:

    #. FIELD_NC - Full model domain output for the selected month (very large files).
    #. FREQ_NC - Data from a subset of model nodes providing the wave parameters derived from the frequency spectra.
    #. SPEC_NC - Data from a subset of model nodes provide 2-D wave spectra.

The on-line `ResourceCode`_ tools can be used to identify data locations of interest prior to 
downloading specific file sets.

.. _ResourceCode: https://resourcecode.ifremer.fr/
.. _HINDCAST: https://www.umr-lops.fr/Donnees/Vagues/sextant#/metadata/d089a801-c853-49bd-9064-dde5808ff8d8
.. _PuTTY: https://github.com/github/putty
.. _WinSCP: https://winscp.net/eng/index.php
.. _FileZilla: https://filezilla-project.org/
.. _MobaXTerm: https://mobaxterm.mobatek.net/

`CMEMS`_ - Copernicus Marine Service
########################################

CMEMS data can be obtained from http://www.marineinsitu.eu/dashboard/.
You have to register before the data can be download, 
see http://www.marineinsitu.eu/access-data/ for details on registration.

.. _CMEMS: https://marine.copernicus.eu/