.. _aboutwaveval:

About WaveVal
=============

The general motivation for this package is the validation of numerical wave simulation models against observed *in situ* 
wavebuoy measurements. The focus is on standard integrated wave parameters that are generated from spectral moments. 
Data readers have been constructed for records archived in the netCDF4 format following published format conventions. A 
generic netCDF class is included to allow the development of readers for data archived using different data conventions. 
The validataion process has been modularised to allow the automated validation of a long-term hindcast model archive 
against a large set of buoy data across the model domain. Methods for matching the model data extracts to the a specific 
buoy record are include. The data matching requires suffcient information in the model extracts to link a given extract 
record to a specific buoy location.

The WaveVal code is developed and maintained by the IES, School of Engineering, at The University of Edinburgh, UK.

Licence
#######

© 2022. University of Edinburgh. All rights reserved. This software was produced for The University of Edinburgh under the 
OCEANERA-NET COFUND ResourceCode project (EC/GA #731200), funded by Scottish Enterprise. All rights in the software are 
reserved by the University of Edinburgh. The University of Edinburgh is granted, for itself and others acting on its behalf, 
a nonexclusive, paid-up, irrevocable worldwide licence in this material to reproduce, prepare derivative works, distribute 
copies to the public, perform publicly and display publicly, and to permit others to do so.

This software is open source under the BSD-3 Licence. Redistribution and use in source and binary forms, with or without
modification, is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following 
   disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
   disclaimer in the documentation and/or other materials provided with the distribution. 
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote 
   products derived from this software without specific prior written permission. 

**Disclaimer**

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.