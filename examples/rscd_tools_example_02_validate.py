#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
from waveval.MatchUpDatabase import csv2tuples, getPlatformRecords, getPlatformList
from waveval.Validation import validate_records
from waveval.Validation import save_tabulated_results

# ----------------------------------------------------------------------------
# Example 2: Calculate validation statistics for match records
# ----------------------------------------------------------------------------

# ================= Read Matched Records CSV File ===========================
csv_path = './data/MATCHUPDB'
csv_fname = 'rscd_matchup.csv'
# Load matched data records from CSV file
matched_records = csv2tuples(csv_path+'/'+csv_fname)
# Get list of uniques platform names
platforms = getPlatformList(matched_records)
# Define wavebuoy netCDF data standard used
buoyFmt = 'InSituTAC'

# ===================== Process Each Platform ===============================
for platform in platforms:
    # Extract matched data records for current platform
    records = getPlatformRecords(matched_records,platform)

    # ========================= Process Record ==============================
    fields = [0,1]
    for field in fields:
        # Process match up records
        # Choose variable to process
        if field == 0:
            oVarName = 'Hm0'      # Significant wave height
        if field == 1:
            oVarName = 'Tp'       # Peak wave period
        if field == 2:
            oVarName = 'Tm02'     # Mean zero crossing period
        if field == 3:
            oVarName = 'Dir'      # Wave direction
        if field == 4:
            oVarName = 'Spr'      # Wave directional spreading
        
        # Set MDB variable names
        if oVarName == 'Hm0':
            mVarName = 'hs'
            varOptions = ['VHM0']
        elif oVarName == 'Tp':
            mVarName = 'fp'
            varOptions = ['VTPK']
        elif oVarName == 'Tm02':
            mVarName = 'f02'
            varOptions = ['VTM02']
        elif oVarName == 'Dir':
            mVarName = 'dir'
            varOptions = ['VPED']
        elif oVarName == 'Spr':
            mVarName = 'spr'
            varOptions = ['VPSP']
        
        # ================== Generate Validation Stats =======================
        results_dir = './data/VALIDATION'
        plot_results = True
        n_valid, valid_stats = validate_records(records,
                                                buoyFmt, platform,
                                                mVarName, varOptions,
                                                oVarName,
                                                results_dir,
                                                plot_results)
    
        # ================== Display Tabulated Results =======================
        if n_valid > 0:
            save_tabulated_results(valid_stats, platform, oVarName, results_dir)
        else:
            print('No validations statistics generated from match up records.')
        
