### Examples

The python scripts give examples of how to generate a matchup database and to carry out a 
validation using the data supplied in the data directory, and provide functional scripts for
running a batch validation job on the IFREMER Datarmor HPC system.
