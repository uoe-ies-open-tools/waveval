#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#   IMPORT PACKAGE DEPENDENCIES
# ----------------------------------------------------------------------------
# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from waveval.MatchUpDatabase import csv2tuples
from waveval.WaveStats import get_buoy_locations
from waveval.WaveStats import get_parameter_stats_global
from waveval.WaveStats import get_parameter_stats_by_buoy
from waveval.Graphics import Regional_Weighted
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
paramStr = ['Hm0','Tp','Tm02','Dir','Spr']
metricStr = ['R','MB','NMB','MAE','NMAE','RMSE','NRMSE','SI']

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

dpath = 'E:/ResourceCode/Data/VALIDATION/RSCD_v3/INSITUTAC'
ppath = 'E:/ResourceCode/Data/VALIDATION/RSCD_v3/WAVEVAL/MAPS'
pfnam = 'INSITUTAC_TS_'

paramStr = ['Hm0','Tp']
metricStr = ['R','NMB','NRMSE']
savePlot = True

for param in paramStr:
    print('PARAMETER: '+param)
    get_parameter_stats_global(dpath, param, metricStr)

recfile = 'E:/ResourceCode/Data/VALIDATION/RSCD_v3/rscd_2017_matchup_v03.csv'
records = csv2tuples(recfile)
buoyLoc = get_buoy_locations(records)

lat = np.asarray(buoyLoc['latitude'],dtype=float)
lon = np.asarray(buoyLoc['longitude'],dtype=float)
platforms = buoyLoc['platform']

for i in range(len(platforms)):
    platforms[i] = platforms[i].strip()

for param in paramStr:
    
    for metric in metricStr:
        
        stats = get_parameter_stats_by_buoy(dpath, param, metric)
        
        pfrm = stats['platform']
        wght = stats['mean']
        dom = [-15, 15, 35, 70]
        pltTitle = 'IN SITU TAC (TS):  Parameter: '+param+',  Metric: '+metric
        
        idx = np.zeros((len(pfrm),), dtype=int)
        for i,p in enumerate(pfrm):
            try:
                indx = platforms.index(p)
            except:
                continue
            idx[i] = indx
        if metric in ['R']:
            clims = [0.0, 1.0]
        elif metric in ['NMB', 'NMAE', 'NRMSE', 'SI']:
            clims = [0.0, 100]
        else:
            clims = [None]
        pname = pfnam+param+'_'+metric+'_by_buoy'
        Regional_Weighted(list(lat[idx]), list(lon[idx]), wght, dom, clims, 
                          pltTitle, ppath, pname, savePlot)

