#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
from waveval import MatchUpDatabase as mdb

# ----------------------------------------------------------------------------
# Exampl 1: Build Match Up Database
# ----------------------------------------------------------------------------

# ========================= Define Database ===============================
db_path = './data/MATCHUPDB'
db_name = 'rscd_matchup'

# ==================== Construct File Filter ==============================
scover = mdb.spatialCoverage(36.00104, 62.99896, -11.998958, 13.489299)
bfilt = mdb.recordFilter([scover])

# ===================== Construct Match-up Database =======================
modDataPath = './data/RSCD_HINDCAST/2017'
modDataset = 'FREQ_NC'
months = []

obsDataPath = './data/INSITU_GLO_WAVE/MO'
obsDataset = 'TS'
obsDataFmt = 'InSituTAC'

rscd_cnt, buoy_cnt = mdb.construct_rscd_mdb(
    db_path, db_name,
    modDataPath, modDataset, months,
    obsDataPath, obsDataset, obsDataFmt, 
    bfilt)

print('MatchUp DB Created:', rscd_cnt, ' model records,',
      buoy_cnt, 'buoy records added.')

# =============== Create Matched Records CSV File =========================
csv_path = db_path
csv_fname = db_name+'.csv'
mdb.db_match_to_csv(db_path, db_name, csv_path, csv_fname)

records = mdb.csv2tuples(csv_path+'/'+csv_fname)

# Display records to be processed
print(records)