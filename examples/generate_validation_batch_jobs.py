#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

start_rec = 0
num_recs = 154
baseFile = 'base_insitutac.pbs'

for irec in range(start_rec, num_recs):
    inFile = open(baseFile,'r')
    ofname = 'insitutac_job_'+str(irec).zfill(3)+'.pbs'
    outFile = open(ofname,'w')
    for astr in inFile:
        if 'python3' in astr:
            astr = astr.strip('\n')+' -n '+str(irec)+'\n'
        outFile.write(astr)
    inFile.close()
    outFile.close()
    procstr = 'qsub '+ofname
    os.system(procstr)
