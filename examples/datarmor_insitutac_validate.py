#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#   IMPORT PACKAGE DEPENDENCIES
# ----------------------------------------------------------------------------
import sys
import os
import getopt
from waveval.MatchUpDatabase import csv2tuples, mapRecord
from waveval.Validation import validate_records, save_tabulated_results

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    
    minYear = 1994
    maxYear = 2019
    genPlots = False

    try:
        opts, args = getopt.getopt(argv,'hp:f:n:',["--path","--fname=",
                                                   "--recnum","minYear=",
                                                   "maxYear=","genPlots="])
    except getopt.GetoptError:
        print('python3 datarmor_insitutac_validate.py -p <path> -f <fname> -n <recnum>')
        sys.exit(4)
    if opts == []:
        print('python3 datarmor_insitutac_validate.py -p <path> -f <fname> -n <recnum>')
        sys.exit(4)
    for opt, arg in opts:
        print(opt,arg)
        if opt == '-h':
            print('python3 datarmor_insitutac_validate.py -p <path> -f <fname> -n <recnum>')
            sys.exit()
        elif opt in ("-p","--path"):
            dat_path = arg
        elif opt in ("-f","--fname"):
            rec_file = arg
        elif opt in ("-n","--recnum"):
            recNum = int(arg)
        elif opt in ("--minYear"):
            minYear = int(arg)
        elif opt in ("--maxYear"):
            maxYear = int(arg)
        elif opt in ("--genPlots"):
            if arg[0].upper() == "T":
                genPlots = True

    # =================== Get Records to Process =============================
    rec_list = csv2tuples(os.path.join(dat_path,rec_file))
    
    rec = rec_list[recNum]
    yearBgn = int(rec[6].split('-')[0])
    if yearBgn < minYear:
        yearBgn = minYear
    yearEnd = int(rec[7].split('-')[0])
    if yearEnd > maxYear:
        yearEnd = maxYear
    records = mapRecord(yearBgn,yearEnd,rec)
    platform = records[0][8]
    
    buoyFmt = 'InSituTAC'
    
    # ========================= Process Record ==============================
    # Choose fields to process
    fields = [0,1,3,4]
    
    for field in fields:
        # Get internal variable name string
        if field == 0:
            oVarName = 'Hm0'      # Significant wave height
        if field == 1:
            oVarName = 'Tp'       # Peak wave period
        if field == 2:
            oVarName = 'Tm02'     # Mean zero crossing period
        if field == 3:
            oVarName = 'Dir'      # Wave direction
        if field == 4:
            oVarName = 'Spr'      # Wave directional spreading
        
        # Set MDB variable names
        if oVarName == 'Hm0':
            mVarName = 'hs'
            varOptions = ['VHM0']
        elif oVarName == 'Tp':
            mVarName = 'fp'
            varOptions = ['VTPK']
        elif oVarName == 'Tm02':
            mVarName = 'f02'
            varOptions = ['VTM02']
        elif oVarName == 'Dir':
            mVarName = 'dir'
            varOptions = ['VPED']
        elif oVarName == 'Spr':
            mVarName = 'spr'
            varOptions = ['VPSP']
        
        # ================== Generate Validation Stats ===========================
        plot_results = genPlots
        n_valid, valid_stats = validate_records(records,
                                         buoyFmt, platform,
                                         mVarName, varOptions,
                                         oVarName,
                                         dat_path,
                                         plot_results)

        # ================== Save Tabulated Results ===========================
        if n_valid > 0:
            save_tabulated_results(valid_stats, platform, oVarName, dat_path)
        else:
            print('No validations statistics generated from match up records.')
    
#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------
if __name__ == "__main__":
    main(sys.argv[1:])
