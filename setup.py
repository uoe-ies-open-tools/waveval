import setuptools
from setuptools import setup

setup(name='waveval',
      version='1.1.2',
      description='Wave model validation tools',
      url='https://git.ecdf.ed.ac.uk/uoe-ies-open-tools/waveval',
      author='Chris Old',
      author_email='',
      license='BSD',
      packages=setuptools.find_packages(),
      zip_safe=False,
      python_requires='>=3.7',
      install_requires=[
            'netcdf4',
            'cartopy',
            'pyshp',
            'shapely',
            'geos',
            'matplotlib',
            'scipy',
            'numpy',
            ]
      )
