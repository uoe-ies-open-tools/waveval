# WaveVal
Wave model validation tools: calculation of statistical validation metrics for integrated wave parameters.
Tools developed under the OPENERA-NET ResourceCode project (EC/GA #731200).

<a href="https://doi.org/10.5281/zenodo.6451437"><img src="https://zenodo.org/badge/DOI/10.5281/zenodo.6451437.svg" alt="DOI"></a>

### Purpose
These tools were developed to validate the [ResourceCode](https://resourcecode.ifremer.fr/) WaveWatch III hindcast data set against CMEMS InsituTAC wavebuoy archive data. The tools focus on validataion against integrated wave parameters calculated from the wave spectra (e.g. Hm0, Tp, Tm02, etc.). The underlying data format supported is netCDF4, this is the standard used to archive the majority of oceanographic data, and has been used for the hindcast archive. The tools have been modularised to allow future development, and extension to other data formats.

### Documentation
Current documentation is at [Read the Docs](https://waveval.readthedocs.io/en/latest/).
The documentation contains a workflow guide that explains the validation process as applied to the ResourceCode hindcast, a description of internal data structure formats used, descriptions of examples of use, a guide to run batch validataion jobs on the IFREMER Datarmor HPC system, amnd data access information.

### Examples
Basic usage is demonstrated in the Examples directory. 
The examples are explained in the documentation. 
After looking at the documentation, check out the examples.

### Install package 
For cleaner package management and to avoid conflicts between different versions of packages, we recommend installing inside an Anaconda or pip environment.
However, this is not required.

First, pull down the current source code from either by downloading a zip file or using `git clone`.

Included in the repository is an environment.yml file that can be used to build a conda environment for installing the WaveVal package. To build the conda environment in an Anaconda shell change to the WaveVal directory where the source code has be downloaded, then run the command:

        conda env create -f environment.yml
        
This will build a conda environment called `wavevalenv`. To check that the environment has been built use:

        conda env list
        
The environment `wavevalenv` should be in the list. 

To activate this environment use:

        conda activate wavevalenv

The `waveval` package is install from the from the command line as follows: while in the main WaveVal directory, use the command:

        pip install -e .[waveval]

The `-e` flag signals developer mode, meaning that if you update the code from Github, your installation will automatically take those changes into account without requiring re-installation.
Some other essential packages used in WaveVal may be installed if they do not exist in your system or environment.

If you encounter problems with the above install method, you may try to install dependencies manually before installing WaveVal.
First, ensure you have a recent version of Python (greater than 3.5).
Then, install packages `numpy`, `scipy`, `matplotlib`, `netCDF4`, and `cartopy`.        

### Citing WaveVal
Using WaveVal in your work? Cite as:

Chris Old.
uoe-ies/WaveVal. Zenodo. https://doi.org/10.5281/zenodo.6451437 

---

© 2022. University of Edinburgh. All rights reserved. This software was produced for The University of Edinburgh under the 
OCEANERA-NET COFUND ResourceCode project (EC/GA #731200), funded by Scottish Enterprise. All rights in the software are 
reserved by the University of Edinburgh. The University of Edinburgh is granted, for itself and others acting on its behalf, 
a nonexclusive, paid-up, irrevocable worldwide licence in this material to reproduce, prepare derivative works, distribute 
copies to the public, perform publicly and display publicly, and to permit others to do so.

This software is open source under the BSD-3 Licence. Redistribution and use in source and binary forms, with or without
modification, is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following 
   disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
   disclaimer in the documentation and/or other materials provided with the distribution. 
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote 
   products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
