# -*- coding: utf-8 -*-
"""
Generic base data format class structures for building loaders for specific 
data set.

:Dependencies [External]: os, numpy, netCDF4
:Dependencies [Internal]: 

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import numpy as np
from netCDF4 import Dataset as ncdata
# Local Module Dependencies
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------
class netcdfGeneric:
    """
    Base class for loading and interogating netCDF4 data files.
    """
    def __init__(self, fileName):

        self.file = {}

        if os.path.isfile(fileName):
            self.file.update({'name': fileName})
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                self.file.update({'attribs': nc.ncattrs()})
                dimNames = []
                for key in nc.dimensions.keys():
                    dimNames.append(nc.dimensions[key].name)
                self.file.update({'dims': dimNames})
                self.file.update({'vars': list(nc.variables)})
        else:
            self.file.update({'name': ''})
            self.file.update({'attribs': []})
            self.file.update({'vars': []})
            print('WARNING: File '+fileName+' does not exist.')

    def listGlobalAttr(self):
        with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
            attrList = nc.ncattrs()
        return attrList

    def hasGlobalAttr(self, attrName):
        return attrName in self.file['attribs']

    def getGlobalAttr(self, attrName):
        if self.hasGlobalAttr(attrName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                attrib = nc.__getattr__(attrName)
        else:
            print('WARNING: Global Attribute '+attrName+' does not exist.')
            print('Available global attributes:')
            print(list(self.file['attribs']))
            attrib = None
        return attrib

    def listDims(self):
        dimsList = []
        with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
            for key in nc.dimensions.keys():
                dimsList.append(nc.dimensions[key].name)
        return dimsList
        
    def hasDim(self, dimName):
        return dimName in self.file['dims']

    def getDim(self, dimName):
        if self.hasDim(dimName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                dimSize = nc.dimensions[dimName].size
        else:
            print('WARNING: Dimension '+dimName+' does not exist.')
            print('Available dimensions:')
            print(list(self.file['dims']))
            dimSize = None
        return dimSize

    def listVars(self):
        with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
            varList = list(nc.variables)
        return varList

    def hasVar(self, varName):
        return varName in self.file['vars']

    def getVarAttrList(self, varName):
        if self.hasVar(varName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                attrList = nc.variables[varName].ncattrs()
        else:
            print('WARNING: Variable '+varName+' does not exist.')
            print('Available variables:')
            print(list(self.file['vars']))
            attrList = []
        return attrList

    def hasAttr(self, varName, attrName):
        attrList = self.getVarAttrList(varName)
        return attrName in attrList

    def getVarAttr(self, varName, attrName):
        if varName in self.file['vars']:
            if self.hasAttr(varName, attrName):
                with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                    attr = nc.variables[varName].getncattr(attrName)
            else:
                print('WARNING: Attribute '+attrName+' does not exist.')
                print('Available attributes:')
                print(list(self.getVarAttrList(varName)))
                attr = None
        else:
            print('WARNING: Variable '+varName+' does not exist.')
            print('Available variables:')
            print(list(self.file['vars']))
            attr = None
        return attr

    def getVarDimNames(self, varName):
        if self.hasVar(varName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                dimNames = list(nc.variables[varName].dimensions)
        else:
            print('WARNING: Variable '+varName+' does not exist.')
            print('Available variables:')
            print(list(self.file['vars']))
            dimNames = None
        return dimNames

    def getVarDimSizes(self, varName):
        if self.hasVar(varName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                dimSizes = list(nc.variables[varName].shape)
        else:
            print('WARNING: Variable '+varName+' does not exist.')
            print('Available variables:')
            print(list(self.file['vars']))
            dimSizes = None
        return dimSizes

    def getVar(self, varName):
        if self.hasVar(varName):
            with ncdata(self.file['name'], 'r', format='NETCDF4') as nc:
                if np.ma.isMaskedArray(nc.variables[varName][:]):
                    var = np.ma.getdata(nc.variables[varName][:])
                else:
                    var = nc.variables[varName][:]
                if var.dtype == '|S1':
                    var = (b''.join(list(var))).decode('utf-8')
        else:
            print('WARNING: Variable '+varName+' does not exist.')
            print('Available variables:')
            print(list(self.file['vars']))
            var = None
        return var


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


