# -*- coding: utf-8 -*-
"""
Internal date/time manipulation functions.

These functions are used to standardise the time stamping between data types.

:Dependencies [External]: datetime, netCDF4
:Dependencies [Internal]: 

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
from datetime import datetime
# Non-Standard Python Dependencies
import numpy as np
from netCDF4 import num2date, date2num
# Local Module Dependencies
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
stdTimeUnits = 'days since 0001-01-01 00:00:00'
stdTimeFmt = "%Y-%m-%d %H:%M:%S"
stdCalendar = 'gregorian'

# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------
class temporalCoverage:

    def __init__(self, timeStart: str, timeEnd: str):
        self.timeRangeStr = [timeStart, timeEnd]
        self.timeRangeNum = [dateNumFromStr(timeStart),
                             dateNumFromStr(timeEnd)]
        self.units = stdTimeUnits
        self.format = stdTimeFmt
        self.calendar = stdCalendar

    def timeOverlap(self, tRec):
        if len(tRec) > 0:
            t0 = tRec[0]
            t1 = tRec[-1]
            case01 = (t0 >= min(self.timeRangeNum)) & \
                (t1 <= max(self.timeRangeNum))
            case02 = (t0 < min(self.timeRangeNum)) & \
                (t1 >= min(self.timeRangeNum))
            case03 = (t0 < max(self.timeRangeNum)) & \
                (t1 >= max(self.timeRangeNum))
            case04 = (t0 < min(self.timeRangeNum)) & \
                (t1 > max(self.timeRangeNum))
            overlaps = case01 or case02 or case03 or case04
            if overlaps:
                tindx0 = np.where(tRec >= self.timeRangeNum[0])[0][0]
                tindx1 = np.where(tRec <= self.timeRangeNum[1])[0][-1]
                indices = [tindx0, tindx1]
            else:
                indices = [None, None]
        else:
            overlaps = False
            indices = [None, None]
        return overlaps, indices


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def dateStr(dateNum):
    if dateNum.size == 1:
        date = num2date(dateNum, stdTimeUnits).strftime()
    else:
        date = [num2date(d, stdTimeUnits).strftime() for d in dateNum]
    return date


def dateNumFromStr(dateStr):
    dt = datetime.strptime(dateStr, stdTimeFmt)
    dateNum = date2num(dt, stdTimeUnits, stdCalendar)
    return dateNum


def matlab2std(dateNum, isLeapYear: bool):
    # need check to ensure dateNum is in days
    if isLeapYear:
        dn = dateNum - 366.0
    else:
        dn = dateNum - 365.0
    return dn


def std2matlab(dateNum, isLeapYear: bool):
    # need check to ensure dateNum is in days
    if isLeapYear:
        dn = dateNum + 366.0
    else:
        dn = dateNum + 365.0
    return dn


def isLeapYear(year):
    if year % 400 == 0:
        return True
    if year % 100 == 0:
        return False
    if year % 4 == 0:
        return True
    else:
        return False

